#!/usr/bin/perl
use strict;
use Data::Dumper;
use POSIX;
use LWP;

# setup: as root: apt install libwww-perl

my $URL='https://pkgmaster.devuan.org/mirror_list.txt';

my $ua=LWP::UserAgent->new;
my $resp=$ua->get($URL) or die "Could't get the mirror list: [$URL]\n";
my $data=$resp->content();

die "Got an empty $URL...\n" if $data eq "";

my @data;
my %entry;
my $line_num;
for (split /\n/,$data) {
	$line_num++;
	if (/\S/) {
		$entry{'LINE'}||="LINE $line_num";
		$entry{$1}=$2 if /^([^:]+):\s+(.+)/ or die "Invalid syntax on line $line_num [$_]\n";
	} else {
		push @data,{%entry} if $entry{'LINE'};
		%entry=();
		next;
	}
}
push @data,{%entry} if $entry{'LINE'};

my %iso3166cc=(
'AD'=>[],
'AE'=>[],
'AF'=>[],
'AG'=>[],
'AI'=>[],
'AL'=>[],
'AM'=>[],
'AO'=>[],
'AQ'=>[],
'AR'=>[],
'AS'=>[],
'AT'=>[],
'AU'=>[],
'AW'=>[],
'AX'=>[],
'AZ'=>[],
'BA'=>[],
'BB'=>[],
'BD'=>[],
'BE'=>[],
'BF'=>[],
'BG'=>[],
'BH'=>[],
'BI'=>[],
'BJ'=>[],
'BL'=>[],
'BM'=>[],
'BN'=>[],
'BO'=>[],
'BQ'=>[],
'BR'=>[],
'BS'=>[],
'BT'=>[],
'BV'=>[],
'BW'=>[],
'BY'=>[],
'BZ'=>[],
'CA'=>[],
'CC'=>[],
'CD'=>[],
'CF'=>[],
'CG'=>[],
'CH'=>[],
'CI'=>[],
'CK'=>[],
'CL'=>[],
'CM'=>[],
'CN'=>[],
'CO'=>[],
'CR'=>[],
'CU'=>[],
'CV'=>[],
'CW'=>[],
'CX'=>[],
'CY'=>[],
'CZ'=>[],
'DE'=>[],
'DJ'=>[],
'DK'=>[],
'DM'=>[],
'DO'=>[],
'DZ'=>[],
'EC'=>[],
'EE'=>[],
'EG'=>[],
'ER'=>[],
'ES'=>[],
'ET'=>[],
'FI'=>[],
'FJ'=>[],
'FK'=>[],
'FM'=>[],
'FO'=>[],
'FR'=>[],
'GA'=>[],
'GB'=>[],
'GD'=>[],
'GE'=>[],
'GF'=>[],
'GG'=>[],
'GH'=>[],
'GI'=>[],
'GL'=>[],
'GM'=>[],
'GN'=>[],
'GP'=>[],
'GQ'=>[],
'GR'=>[],
'GS'=>[],
'GT'=>[],
'GU'=>[],
'GW'=>[],
'GY'=>[],
'HK'=>[],
'HM'=>[],
'HN'=>[],
'HR'=>[],
'HT'=>[],
'HU'=>[],
'ID'=>[],
'IE'=>[],
'IL'=>[],
'IM'=>[],
'IN'=>[],
'IO'=>[],
'IQ'=>[],
'IR'=>[],
'IS'=>[],
'IT'=>[],
'JE'=>[],
'JM'=>[],
'JO'=>[],
'JP'=>[],
'KE'=>[],
'KG'=>[],
'KH'=>[],
'KI'=>[],
'KM'=>[],
'KN'=>[],
'KP'=>[],
'KR'=>[],
'KW'=>[],
'KY'=>[],
'KZ'=>[],
'LA'=>[],
'LB'=>[],
'LC'=>[],
'LI'=>[],
'LK'=>[],
'LR'=>[],
'LS'=>[],
'LT'=>[],
'LU'=>[],
'LV'=>[],
'LY'=>[],
'MA'=>[],
'MC'=>[],
'MD'=>[],
'ME'=>[],
'MF'=>[],
'MG'=>[],
'MH'=>[],
'MK'=>[],
'ML'=>[],
'MM'=>[],
'MN'=>[],
'MO'=>[],
'MP'=>[],
'MQ'=>[],
'MR'=>[],
'MS'=>[],
'MT'=>[],
'MU'=>[],
'MV'=>[],
'MW'=>[],
'MX'=>[],
'MY'=>[],
'MZ'=>[],
'NA'=>[],
'NC'=>[],
'NE'=>[],
'NF'=>[],
'NG'=>[],
'NI'=>[],
'NL'=>[],
'NO'=>[],
'NP'=>[],
'NR'=>[],
'NU'=>[],
'NZ'=>[],
'OM'=>[],
'PA'=>[],
'PE'=>[],
'PF'=>[],
'PG'=>[],
'PH'=>[],
'PK'=>[],
'PL'=>[],
'PM'=>[],
'PN'=>[],
'PR'=>[],
'PS'=>[],
'PT'=>[],
'PW'=>[],
'PY'=>[],
'QA'=>[],
'RE'=>[],
'RO'=>[],
'RS'=>[],
'RU'=>[],
'RW'=>[],
'SA'=>[],
'SB'=>[],
'SC'=>[],
'SD'=>[],
'SE'=>[],
'SG'=>[],
'SH'=>[],
'SI'=>[],
'SJ'=>[],
'SK'=>[],
'SL'=>[],
'SM'=>[],
'SN'=>[],
'SO'=>[],
'SR'=>[],
'SS'=>[],
'ST'=>[],
'SV'=>[],
'SX'=>[],
'SY'=>[],
'SZ'=>[],
'TC'=>[],
'TD'=>[],
'TF'=>[],
'TG'=>[],
'TH'=>[],
'TJ'=>[],
'TK'=>[],
'TL'=>[],
'TM'=>[],
'TN'=>[],
'TO'=>[],
'TR'=>[],
'TT'=>[],
'TV'=>[],
'TW'=>[],
'TZ'=>[],
'UA'=>[],
'UG'=>[],
'UM'=>[],
'US'=>[],
'UY'=>[],
'UZ'=>[],
'VA'=>[],
'VC'=>[],
'VE'=>[],
'VG'=>[],
'VI'=>[],
'VN'=>[],
'VU'=>[],
'WF'=>[],
'WS'=>[],
'YE'=>[],
'YT'=>[],
'ZA'=>[],
'ZM'=>[],
'ZW'=>[],
);

my %check_fields=(FQDN=>'LINE',Active =>'FQDN',BaseURL=>'FQDN','CountryCode'=>'FQDN');
my %check_values=(Active=>'yes','CountryCode'=>'[a-z][a-z](\s*\|\s*[a-z][a-z])*');
DATA:
for my $ent(@data) {
	for(keys %check_fields) {
		next if $ent->{$_};
		next DATA;
	}
	for(keys %check_values) {
		next if $ent->{$_}=~/^$check_values{$_}$/i;
		next DATA;
	}
	next if $ent->{'FQDN'} eq 'pkgmaster.devuan.org';
	push @{$iso3166cc{$_}},"http://$ent->{BaseURL}" for split /\s*\|\s*/,$ent->{'CountryCode'};
}
#while(my($cc,$a)=each %iso3166cc) {
for my $cc (sort keys %iso3166cc) {
	my $a=$iso3166cc{$cc};
	my $cl=lc($cc);
	print "#LOC:$cc\nhttp://$cl.deb.devuan.org/merged/\n";
	for my $u(values @$a) {
		$u.='/' if $u!~/\/$/;
		print "$u\n";
	}
}

exit(0);
